var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var customError = require('./utils/custom-error');
var passport = require('./config/passport');

var indexRouter = require('./routes/index')(passport);
var usersRouter = require('./routes/users')(passport);
var rolesRouter = require('./routes/roles')(passport);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.set('passport', passport);

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/roles', rolesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new customError.serverError.NotFound();
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  if(!err.statusCode) return next(err);

  res.status(err.statusCode).json(err);
});

//Last middleware - No unexpected error should be returned to the user
app.use(function(err, req, res, next) {
  console.log(err);
  res.status(500).json(new customError.serverError.Generic());
});

module.exports = app;
