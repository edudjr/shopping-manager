'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      userId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name:{
        type: Sequelize.TEXT,
        allowNull: false
      },
      address:{
        type: Sequelize.TEXT
      },
      email:{
        type: Sequelize.TEXT,
        allowNull: false
      },
      password:{
        type: Sequelize.TEXT,
        allowNull: false
      },
      token:{
        type: Sequelize.TEXT
      },
      cnpj:{
        type: Sequelize.STRING
      },
      createdAt: {
        type: Sequelize.DATE
      },
      updatedAt: {
        type: Sequelize.DATE
      },
      roleId: {
          type: Sequelize.INTEGER,
          references: {
              model: 'Roles',
              key: 'roleId'
          },
          onUpdate: 'cascade',
          onDelete: 'set null'
      },
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users')
  }
};
