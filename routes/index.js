var express = require('express');
var router = express.Router();

module.exports = function(passport) {
  /* GET home page. */
  router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
  });
  
  router.post('/login',
    passport.authenticate('local', {session: false}),
    function(req, res) {
      res.status(200).json(req.user);
    });
    
  return router;
};
