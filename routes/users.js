var express = require('express');
var router = express.Router();
var UserController = require('../controllers/user-controller');

module.exports = function(passport) {
  router.get('/',function(req, res, next){
    passport.authenticate('bearer', function(err, user, info) {
      if (err) return next(err);
      if (!user) throw new customError.user.InvalidToken();

      res.status(200).json(user);
    })(req, res, next);
  });
  
  router.post('/', function(req, res, next) {
    var params = req.body;
    UserController.create(params)
      .then(function(role){
        res.status(201).json(role);
      })
      .catch(function(err){
        next(err);
      })
  });
  
  return router;
};
