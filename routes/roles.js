var express = require('express');
var RoleController = require('../controllers/role-controller');
var router = express.Router();

module.exports = function(passport) {
  router.post('/', function(req, res, next) {
    var params = req.body;
    RoleController.create(params)
      .then(function(role){
        res.status(201).json(role);
      })
      .catch(function(err){
        next(err);
      })
  });
  
  return router;
};
