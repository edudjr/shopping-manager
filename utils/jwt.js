var jwt = require('jsonwebtoken');
var fs = require('fs');
var path = require('path');
var secret = fs.readFileSync(path.join(__dirname,'../.certs/secret.key'),'utf8');
var CustomError = require('./custom-error');

module.exports = {
  sign: function(payload){
    return new Promise(function(resolve, reject){
      var data = {
        issuer: 'ShoppingManager',
        payload: payload
      }
      jwt.sign(data, secret, function(err, token){
        if(err) return reject(err);
        resolve(token);
      });
    });
  },
  verify: function(token){
    return new Promise(function(resolve, reject){
      // verify a token symmetric
      jwt.verify(token, secret, function(err, decoded) {
        if(err) return reject(new CustomError.user.InvalidToken());
        resolve(decoded);
      });
    })
  }
}
