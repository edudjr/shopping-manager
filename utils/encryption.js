var bcrypt = require('bcrypt');

const saltRounds = 10;
module.exports = {
  encrypt: function(rawPassword){
    return bcrypt.hash(rawPassword, saltRounds);
  },
  compare: function(rawPassword, encryptedPassword){
    return bcrypt.compare(rawPassword, encryptedPassword);
  }
}
