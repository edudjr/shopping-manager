var CustomError = require('../utils/custom-error');
var _ = require('lodash');

function validateExists(params, name){
  return params.hasOwnProperty(name);
}

function validateProperty(params, name){
  if(!params.hasOwnProperty(name)){
    throw new CustomError.inputValidation.Missing(name);
  }
}

function validateIntegerType(params, name){
  validateProperty(params, name);
  if(!_.isInteger(params[name])){
    throw new CustomError.inputValidation.TypeOfInteger(name);
  }
}

function validateStringType(params, name){
  validateProperty(params, name);
  if(!_.isString(params[name])){
    throw new CustomError.inputValidation.TypeOfString(name);
  }
}

function validateObjectType(params, name){
  validateProperty(params, name);
  if(!_.isObject(params[name])){
    throw new CustomError.inputValidation.TypeOfObject(name);
  }
}

function validateParseInt(param){
  var parsed = parseInt(param);
  if(!_.isInteger(parsed)){
    throw new CustomError.inputValidation.NotValid(param);
  }
  return parsed;
}

function validateDigitsOnly(param){
  return param.replace(/\s/g,'').replace(/\D/g,'');
}

module.exports = {
  property: validateProperty,
  integerType: validateIntegerType,
  stringType: validateStringType,
  parseInt: validateParseInt,
  objectType: validateObjectType,
  exists: validateExists,
  digitsOnly: validateDigitsOnly
}
