class HttpError {
  constructor() {
    this.e400 = {
      statusCode: 400,
      name: 'Bad Request'
    },
    this.e401 = {
      statusCode: 401,
      name: 'Unauthorized'
    },
    this.e403 = {
      statusCode: 403,
      name: 'Forbidden'
    },
    this.e404 = {
      statusCode: 404,
      name: 'Not Found'
    },
    this.e409 = {
      statusCode: 409,
      name: 'Conflict'
    },
    this.e500 = {
      statusCode: 500,
      name: 'Internal Server Error'
    }
  }
}

module.exports = {
  inputValidation: {
    Missing: function(str) {
      Object.assign(this, new HttpError().e400);
      this.message = 'Missing ' + str;
    },
    TypeOfString: function(str) {
      Object.assign(this, new HttpError().e400);
      this.message = str + ' should be a String';
    },
    TypeOfInteger: function(str) {
      Object.assign(this, new HttpError().e400);
      this.message = str + ' should be an Integer';
    },
    TypeOfObject: function(str) {
      Object.assign(this, new HttpError().e400);
      this.message = str + ' should be an Object';
    },
    NotValid: function(str) {
      Object.assign(this, new HttpError().e400);
      this.message = str + ' is not valid';
    }
  },
  user: {
    Authentication: function(){
      Object.assign(this, new HttpError().e401);
      this.message = 'Invalid credentials';
    },
    AlreadyExists: function(){
      Object.assign(this, new HttpError().e409);
      this.message = 'User already exists';
    },
    InvalidToken: function(){
      Object.assign(this, new HttpError().e401);
      this.message = 'Token is not valid';
    },
    NotFound: function(){
      Object.assign(this, new HttpError().e404);
      this.message = 'User was not found';
    }
  },
  roleValidation: {
    InvalidType: function() {
      Object.assign(this, new HttpError().e400);
      this.message = 'Invalid type for role';
    }
  },
  serverError: {
    Generic: function() {
      Object.assign(this, new HttpError().e500);
      this.message = 'Server responded unexpectedly';
    },
    Conflict: function() {
      Object.assign(this, new HttpError().e409);
      this.message = 'Resource already exists';
    },
    NotFound: function() {
      Object.assign(this, new HttpError().e404);
      this.message = 'Resource not found';
    },
    Forbidden: function() {
      Object.assign(this, new HttpError().e403);
      this.message = 'Not allowed to perform this action';
    }
  }
}
