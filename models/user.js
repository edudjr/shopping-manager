"use strict";

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define("User", {
    userId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    address: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    email: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    token: {
      type: DataTypes.TEXT
    },
    cnpj: {
      type: DataTypes.STRING,
      allowNull: true
    }
  });
  
  User.prototype.toJSON = function(){
    var values = Object.assign({}, this.get());
    delete values.password;
    return values;
  }

  return User;
};
