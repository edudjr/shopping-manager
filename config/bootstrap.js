module.exports = {
  associateDb: function(){
    User.belongsTo(Role, {as:'role', foreignKey: {name: 'roleId', allowNull: false}});
  },
  init: function(){
    var roles = [
      'Supervisor', 'Technician', 'Company'
    ];
    for(var i=0; i<roles.length; i++){
      var role = roles[i];
      Role.findOrCreate(
        { where: { description: role } ,
        defaults: { roleId: i, description: role }}
      );
    }
  }
}
