var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var encryption = require('../utils/encryption');
var customError = require('../utils/custom-error');
var jwt = require('../utils/jwt');

passport.use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password'
  },
  function(email, password, done) {
    
    console.log('OOK');
    var user = null;
    User.findOne({where: {email: email}})
      .then(function(instance){
        if(!instance) throw new customError.user.Authentication();
        user = instance;
        return encryption.compare(password, instance.password);
      })
      .then(function(result){
        if(!result) throw new customError.user.Authentication();
        return jwt.sign({email: email});
      })
      .then(function(token){
        return user.update({token: token});
      })
      .then(function(updatedUser){
        done(null, updatedUser);
      })
      .catch(function(err){
        done(err);
      });
  }
));

passport.use(new BearerStrategy(
  function(token, done) {
    // verify a token symmetric
    jwt.verify(token)
      .then(function(decoded){
        return User.findOne({where: {token: token}});
      })
      .then(function(user){
        if(!user) throw new customError.user.InvalidToken();
        done(null, user);
      })
      .catch(function(err){
        done(err);
      });
  }
));

module.exports = passport;
