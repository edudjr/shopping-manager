var roleModel = require('../input-models/role-model');
var customError = require('../utils/custom-error');

module.exports = {
  create: function(params){
    var inputModel = new roleModel.RoleCreateModel(params);
    //Add model
    return Role.findOne({where: {description: inputModel.description}})
      .then(function(role){
        if (role)
          throw new customError.serverError.Conflict();
        return Role.create(inputModel);
      })
  }
}
