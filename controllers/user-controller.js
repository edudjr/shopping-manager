var userModel = require('../input-models/user-model');
var encryption = require('../utils/encryption');
var CustomError = require('../utils/custom-error');
var jwt = require('../utils/jwt');

module.exports = {
  create: function(params){
    var inputModel = new userModel.UserCreateModel(params);

    //Create password hash
    return encryption.encrypt(inputModel.password)
      .then(function(hash){
        inputModel.password = hash;
        return jwt.sign({email: inputModel.email});
      })
      .then(function(token){
        inputModel.token = token;
        return User.create(inputModel);
      })
      .then(function(user){
        return user;
      })
      .catch(function(err){
        if(err.name == 'SequelizeForeignKeyConstraintError'){
          throw new CustomError.roleValidation.InvalidType();
        }
        throw err;
      });
  }
}
