'use strict';
var validation = require('../utils/validation');
var customError = require('../utils/custom-error');

class RoleCreateModel{
  constructor(params){
    //required fields
    validation.integerType(params, 'roleId');
    validation.stringType(params, 'description');
    var description = params.description;
    if (description != 'Technician' &&
        description != 'Supervisor' &&
        description != 'Company')
      throw new customError.roleValidation.InvalidType();

    this.roleId = params.roleId;
    this.description = description;
  }
}

module.exports = {
  RoleCreateModel: RoleCreateModel
}
