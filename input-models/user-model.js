'use strict';
var validation = require('../utils/validation');
var customError = require('../utils/custom-error');

class UserCreateModel{
  constructor(params){
    //required fields
    validation.stringType(params, 'name');
    validation.stringType(params, 'address');
    validation.stringType(params, 'email');
    validation.stringType(params, 'password');
    validation.stringType(params, 'cnpj');
    validation.integerType(params, 'roleId');

    this.name = params.name;
    this.address = params.address;
    this.email = params.email;
    this.password = params.password;
    this.cnpj = params.cnpj;
    this.roleId = params.roleId;
  }
}

module.exports = {
  UserCreateModel: UserCreateModel
}
